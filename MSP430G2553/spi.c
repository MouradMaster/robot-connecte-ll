/*
 * spi.c
 *
 *  Created on: 21 mars 2021
 *      Author: mourad - shesley
 */


#include <spi.h>

/* Get received character data through SPI protocol */
void RXdata_spi(unsigned char *c)
{
    while( (UCB0STAT & UCBUSY) && !(UCB0STAT & UCOE) ){/*....*/};
    while(!(IFG2 & UCB0RXIFG)){/*....*/};
    *c = UCB0RXBUF;
}

/* Send character data through SPI protocol */
void TXdata_spi(unsigned char c)
{
    while ((UCB0STAT & UCBUSY)){/*....*/};
    while(!(IFG2 & UCB0TXIFG)){/*....*/};
    UCB0TXBUF = c;
}



/* Send string throught UART protocol */
void send_msg_SPI(const char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != (char)0x00 ; i++)
    {
        TXdata_spi((unsigned char)msg[i]);
    }
}





