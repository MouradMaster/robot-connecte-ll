#include <msp430g2553.h>
#include <uart.h>
#include <ressources.h>
#include <interpreter.h>
#include <init.h>
#include <motors.h>
#include <spi.h>


static void command_uart_init(void);
static void command_uart_motors(void);
static void command_uart_detection(void);
static void command_spi_motors(void);
static void auto_running(void);
static void scan(void);
static void scan_all(void);
void send_cmd_SPI(char c);


int main(void)
{
    InitBOARD();
    InitUART();
    InitSPI();
    send_cmd_SPI('c');
    __bis_SR_register(LPM4_bits | GIE);

    while(1){

        if(int_UART)
        {
            interpreter_UART();
            command_uart_init();
            if(running){

                if(autorun){
                    auto_running();
                }
                else{
                    command_uart_motors();
                    command_uart_detection();
                    command_spi_motors();
                }
            }
            else{
                stop();
            }
            int_UART= FALSE;
        }
        else
        {
            __bis_SR_register(LPM4_bits | GIE); /* general interrupts enable & Low Power Mode */
        }
    }

}


/* Command interface
 * Inputs : -
 * Outputs : -
 * */

static void command_uart_init(void)
{
      switch(command){
       case HELP:
            display_help();
       break;
       case RUNNING_ON:
            running=TRUE;
            send_msg_UART("\n\rRunning On !\n\r");
       break;
       case RUNNING_OFF:
            running=FALSE;
            send_msg_UART("\n\rRunning Off...\n\r");
       break;
       case MAN:
           autorun=FALSE;
           send_msg_UART("\n\rManual mode\n\r");
      break;
      case AUTO:
           autorun=TRUE;
           send_msg_UART("\n\rAutonomous mode\n\r");
      break;
      case ERR :
            send_msg_UART("\n\rERROR !!! Wrong Input\n\r");
            send_msg_UART("\n\rPress 'h' or tap 'help' to display help\n\r");
       break;
      default:
           /*....*/
       break;
      }
}



/* Motors commands
 * Inputs : -
 * Outputs : -
 * */

static void command_uart_motors(void)
{
       switch(command){
       case FORWARD:
           if(command_2231 != STOP){
           forward();
           send_msg_UART("\n\rMove forward\n\r");
           }
       break;
       case LEFT_ROTATION:
           left_rotation();
           send_msg_UART("\n\rLeft rotation\n\r");
       break;
       case RIGHT_ROTATION:
           right_rotation();
           send_msg_UART("\n\rRight rotation\n\r");
       break;
       case BACK:
           back();
           send_msg_UART("\n\rMove back\n\r");
       break;
       case STEP_FORWARD:
           if(command_2231 != STOP){
           move_forward(70,500);
           send_msg_UART("\n\rStep forward\n\r");
           }
       break;
       case STEP_LEFT_ROTATION:
           move_left_rotation(50,350);
           send_msg_UART("\n\rStep left rotation\n\r");
       break;
       case STEP_RIGHT_ROTATION:
           move_right_rotation(50,350);
           send_msg_UART("\n\rStep right rotation\n\r");
       break;
       case STEP_BACK:
           move_back(70,500);
           send_msg_UART("\n\rStep back\n\r");
       break;
       case STOP:
           stop();
           send_msg_UART("\n\rStop\n\r");
       break;
       default :
           /*....*/
       break;
       }
}

/* Detection commands
 * Inputs : -
 * Outputs : -
 * */

static void command_uart_detection(void){
    switch(command){
       case SCAN:
        send_msg_UART("\n\rScannning...\n\r");
        scan();
       break;
       case SCAN_ALL:
           scan_all();
        break;
       case LEFT_SWEEP:
         send_cmd_SPI('f');
         send_msg_UART("\n\rLeft sweep\n\r");
         (void)display_position_servo(angle_servo);
      break;
     case RIGHT_SWEEP:
          send_cmd_SPI('g');
          send_msg_UART("\n\rRight sweep\n\r");
          (void)display_position_servo(angle_servo);
       break;
      case LEFT_SWEEP_90:
          send_cmd_SPI('x');
          send_msg_UART("\n\rLeft sweep 90 deg\n\r");
          (void)display_position_servo(angle_servo);
       break;
      case RIGHT_SWEEP_90:
           send_cmd_SPI('v');
           send_msg_UART("\n\rRight sweep 90 deg\n\r");
           (void)display_position_servo(angle_servo);
        break;
      case REDRESS:
            send_cmd_SPI('c');
          break;
      default :
         /*....*/
     break;
    }
}

static void command_spi_motors(void){
    switch(command_2231){
    case STOP :
        stop();
    break;
    default :
        /*....*/
    break;
    }
}

static void scan(void){
    send_cmd_SPI('w');
    send_cmd_SPI('w');
    interpreter_SPI();
    if(distance_char>(char)DISTANCE_OBS_MAX)
    {
    send_msg_UART("\n\rNo Obstacle\n\r");
    }
    else
    {
        send_msg_UART("\n\rObstacle to ");
        send_msg_UART((const char *)distance_obs);
        send_msg_UART(" cm \n\r");
    }
}

static void scan_all(void){
    unsigned int i;
    send_cmd_SPI('x');
    delay(1000);
    for(i=0u;i<5u;i++){
        send_msg_UART("\n\n");
        (void)display_position_servo(angle_servo);
        scan();
        send_cmd_SPI('g');
        delay(1000);
    }
    send_cmd_SPI('c');
}

static void auto_running(void){
    unsigned char cmd_auto;
    unsigned char max_distance=(unsigned char)'0';
    ANGLE i=ANG_90;
    ANGLE imax=ANG_90;
    send_cmd_SPI('c');
    delay(1000);
    forward();
    send_msg_UART("\n\rAuto running....\n\r");

   while(autorun){

      scan();
      command_spi_motors();
      if(command_2231==STOP){
          stop();
          scan_all();
          for(i=ANG_180;i<=ANG_0;i++){
              if(max_distance<distance_obs_ang[i]){
                  max_distance=distance_obs_ang[i];
                  imax=i;
              }
          }

          switch(imax){
          case ANG_180 :
              move_left_rotation(50,350);
              move_left_rotation(50,350);
          break;
          case ANG_135 :
              move_left_rotation(50,350);
          break;
          case ANG_45 :
              move_right_rotation(50,350);
          break;
          case ANG_0 :
              move_right_rotation(50,350);
              move_right_rotation(50,350);
          break;
          default :
              move_left_rotation(50,350);
              move_left_rotation(50,350);
          break;
          }

          forward();
      }

      cmd_auto=(unsigned char)UCA0RXBUF;
       if (cmd_auto==(unsigned char)'p')
         {
           autorun=FALSE;
         }

      delay(500);

   }

   send_msg_UART("\n\rStop Autorun !\n\r");
}

void send_cmd_SPI(char c){
    P1OUT &= (~CS); /* Select Device */
    TXdata_spi(c);
    delay(50);
    RXdata_spi(&distance_char);
    P1OUT |= CS; /* Unselect Device */

    /* Update servo angle */
    switch(c){
       case 'x':
           angle_servo=ANG_180;
       break;
       case 'v':
           angle_servo=ANG_0;
       break;
       case 'c':
           angle_servo=ANG_90;
       break;
       case 'f':
           if(angle_servo>ANG_180){
           angle_servo-=1;
           }
       break;
       case 'g':
           if(angle_servo<ANG_0){
           angle_servo+=1;
           }
       break;
       default:
          /*....*/
       break;
    }
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR(void){

    /*UART*/

    if (IFG2 & UCA0RXIFG)
       {
            RXdata(&cmd[num_car]);
            TXdata(cmd[num_car]);
       if( cmd[num_car] == ESC)
       {
            num_car = 0;
            cmd[0] = CR;
            cmd[1] = 0x00;
       }
       else if((cmd[num_car]==LF) || (cmd[num_car]==CR))
       {
           cmd[num_car] = 0x00;
           num_car = 0;
           int_UART=TRUE;
           __bic_SR_register_on_exit(LPM4_bits);
       }
       else if((cmd[num_car] == BSPC) || (cmd[num_car] == DEL))
       {
           cmd[num_car] = 0x00;
           num_car--;
       }
       else if(num_car < CMDLEN_MAX)
       {
           num_car++;
       }
       else
       {
           TXdata(BSPC);
       }

       }


}
