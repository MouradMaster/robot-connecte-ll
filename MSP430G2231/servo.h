/*
 * servo.h
 *
 *  Created on: Mar 23, 2021
 *      Author: Shesley LECKPA
 */

#ifndef SERVO_H_
#define SERVO_H_

#include "typedef.h"

void delay(ULONG time); /* cr�e un retard de l'ordre du ms */
void init_servo(void); /* initialise le servo moteur */
void servo_left(void); /* mets le servomoteur � 180� */
void servo_right(void); /* mets le servomoteur � 0� */
UINT_32 servo_set(UINT_32 angle); /* mets le servomoteur � angle� */




#endif /* SERVO_H_ */








