/*
 * uart.c
 *
 *  Created on: 11 mars 2021
 *  Author: mourad - shesley
 */



#include <uart.h>


/* Get received character data through UART protocol */
void RXdata(unsigned char *c)
{
    while (!(IFG2&UCA0RXIFG)){/*...*/};
    *c = UCA0RXBUF;/*get sent character*/
}

/* Send character data through UART protocol */
void TXdata(unsigned char c)
{
    while (!(IFG2&UCA0TXIFG)){/*...*/};
    UCA0TXBUF = c;;/*send character*/

}

/* Send string throught UART protocol */
void send_msg_UART(const char *msg)
{
    int i = 0;/* char position*/
    for(i=0 ; msg[i] != (char)0x00 ; i++)
    {
        TXdata((unsigned char)msg[i]);
    }
}


/* Send use help message */
void display_help(void){
    send_msg_UART("\n\n\r----------------------HELP------------------------\n\n\r");
    send_msg_UART("\n\n\r-----------Config--------------\n\n\r");
    send_msg_UART("\n\r'a' or 'start' : Activate the running mode\n\r");
    send_msg_UART("\n\r'e' or 'end' : Disable the running mode\n\r");
    send_msg_UART("\n\r'm' or 'manual' : Manual mode\n\r");
    send_msg_UART("\n\r'p' or 'auto' : Autonomous mode\n\r");
    send_msg_UART("\n\r'h' or 'help' : Display this help\n\r");
    send_msg_UART("\n\n\r-----------Moving--------------\n\n\r");
    send_msg_UART("\n\r'b' or 'stop' : Stop moving\n\r");
    send_msg_UART("\n\r'i' or 'forward' : Forward steering\n\r");
    send_msg_UART("\n\r'j' or 'left' : Left rotation steering\n\r");
    send_msg_UART("\n\r'l' or 'right' : Right rotation steering\n\r");
    send_msg_UART("\n\r'k' or 'back' : Back steering\n\r");
    send_msg_UART("\n\r'z' or 'step forward' : Step forward steering\n\r");
    send_msg_UART("\n\r'q' or 'step left' : Step left rotation steering\n\r");
    send_msg_UART("\n\r'd' or 'step right' : Step Right rotation steering\n\r");
    send_msg_UART("\n\r's' or 'step back' : Step back steering\n\r");
    send_msg_UART("\n\n\r-----------Detection--------------\n\n\r");
    send_msg_UART("\n\r'w' or 'scan' : Measure distance\n\r");
    send_msg_UART("\n\r'n' or 'scan all' : Scan all\n\r");
    send_msg_UART("\n\r'x' or 'left sweep 90' : Sweep to left direction\n\r");
    send_msg_UART("\n\r'v' or 'right sweep 90' : Sweep to right direction\n\r");
    send_msg_UART("\n\r'c' or 'redress' : Redress servomotor\n\r");
    send_msg_UART("\n\r'f' or 'left sweep' : Left sweep\n\r");
    send_msg_UART("\n\r'g' or 'right sweep' : Right sweep\n\r");
}

/* Send use help message */
int display_position_servo(ANGLE angle){
    switch(angle){
    case ANG_180:
        send_msg_UART("\n\rWest position\n\r");
    break;
    case ANG_135:
        send_msg_UART("\n\rNorth West position\n\r");
    break;
    case ANG_90:
        send_msg_UART("\n\rNorth position\n\r");
    break;
    case ANG_45:
        send_msg_UART("\n\rNorth East position\n\r");
    break;
    case ANG_0:
        send_msg_UART("\n\rEast position\n\r");
    break;
    default:
        send_msg_UART("\n\rUnknown position\n\r");
    break;

    }
	return 1;
}

