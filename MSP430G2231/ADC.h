/*
 * ADC.h
 *
 *  Created on: Mar 23, 2021
 *      Author: Shesley LECKPA
 */

#ifndef ADC_H_
#define ADC_H_

#include "typedef.h"

void ADC_init(void); /* initialise l'ADC */



void ADC_Demarrer_conversion(UCHAR voie); /* convertit l'entr�e analogique en num�rique */
 


SINT_32 ADC_Lire_resultat (void); /* retourne de r�sultat converti */

SINT_32 ADC_code(UCHAR voie); /* convertit et retourne directement le r�sultat */



#endif /* ADC_H_ */


